This app showcases several techniques for using a foreground service without showing a notification.

### Results

These are accurate as of Android 7.1.

Technique|Removes notification|Service stays in foreground|Removes status bar icon|Earliest working version|Last working version
-|-|-|-|-|-
Invisible notification icon|N|Y|Yes, but it still uses space|4.0 (or earlier)|
Minimum priority|N|Y|Y|4.1|
No notification id|Y|N|Y|4.0 (or earlier)|
No notification icon|Y|Y|Y|4.0 (or earlier)|4.2
Second foreground service|Y|Y|Y|4.0 (or earlier)|7.0
Turn off app notifications|Y|Y|Y|4.1|
