package samuelpalmer.foregroundservicewithoutnotificationicon;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView androidVersion = (TextView) findViewById(R.id.android_version);
        androidVersion.setText("Android: " + Build.VERSION.RELEASE);

        Button normal = (Button)findViewById(R.id.normal);
        Button noid = (Button)findViewById(R.id.noid);
        Button noicon = (Button)findViewById(R.id.noicon);
        Button secondservice = (Button)findViewById(R.id.secondservice);
        Button disableAppNotifications = (Button) findViewById(R.id.disableappnotifications);

        normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAndCheck(ForegroundService.ACTION_NORMAL);
            }
        });

        noid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAndCheck(ForegroundService.ACTION_NO_ID);
            }
        });

        noicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAndCheck(ForegroundService.ACTION_NO_ICON);
            }
        });

        secondservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAndCheck(ForegroundService.ACTION_SECOND_SERVICE);
            }
        });

        disableAppNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(uri);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        startAndCheck(null);
    }

    @Override
    protected void onStop() {
        super.onStop();

        stopService(intent());
    }

    private void startAndCheck(String action) {
        Intent intent = intent().setAction(action);
        if (startService(intent) == null)
            throw new RuntimeException();
    }

    private Intent intent() {
        return new Intent(this, ForegroundService.class);
    }
}
